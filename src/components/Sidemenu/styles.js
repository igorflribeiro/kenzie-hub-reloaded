import styled from 'styled-components'

export const SideMenuContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: blueviolet;
  width: 150px;
  height: 100vh;
  left: 0;
  top: 0;
  position: absolute;
`;