import {useForm} from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import baseURL from '../../services/index'

import {Form, FormButton} from "./styles";
import { TextField} from "@material-ui/core";

import {useLoading} from "../../context/hooks/useLoading";
import {useAuth} from "../../context/hooks/useAuth";

import {useHistory} from 'react-router-dom'

const LoginForm = () => {
    const history = useHistory()

    const {auth, setAuth} = useAuth()
    const {loading, setLoading} = useLoading()

    const schema = yup.object().shape({
     email: yup.string().email('email válido').required('Obrigatório'),
     password: yup.string().required('obrigatório'),
 })

    const {handleSubmit, errors, register, reset} = useForm({
        resolver: yupResolver(schema),
    });

    const handleLogin =  (data) => {
        setLoading(true);
        baseURL.post('/sessions', data).then((response)=>{
            const token = response.data.token
            window.localStorage.clear()
            window.localStorage.setItem('token', JSON.stringify(token))
            history.push('/home')
            setTimeout(()=>setLoading(false), 2500)
        })
    }

    return(
        <>
    <Form onSubmit={handleSubmit(handleLogin)}>
        <TextField
            margin={'normal'}
            variant={"outlined"}
            label={'Email'}
            name={'email'}
            inputRef={register}
            size={'small'}
            error={!!errors.email}
            helperText={errors.email?.message}
        />
        <TextField
            margin={'normal'}
            variant={"outlined"}
            label={'Senha'}
            name={'password'}
            inputRef={register}
            size={'small'}
            error={!!errors.password}
            helperText={errors.password?.message}
        />
        <FormButton type='submit'>Entrar</FormButton>
    </Form>
        </>
    )
}
export default LoginForm