import * as yup from 'yup'
import {useForm} from "react-hook-form";
import {CreateUser} from "../../services/axioshelpers";
import {yupResolver} from '@hookform/resolvers/yup'

import {Form, FormButton} from "./styles";
import { TextField} from "@material-ui/core";

const RegisterForm = ()=>{
    const require = 'Esse campo é obrigatório'

    const schema = yup.object().shape({
        email: yup.string().email('Insira um email válido').required(require),
        password: yup.string().required(require),
        name: yup.string().required(require),
        bio: yup.string().required(require),
        contact: yup.string().required(require),
        course_module: yup.string().required(require),
    })

    const {errors, register, handleSubmit, reset} = useForm({
        resolver: yupResolver(schema),
    })

    const handleRegister = (data) =>{
        console.log(data);
        CreateUser(data)
        reset()
    }

    return(
        <>
    <Form onSubmit={handleSubmit(handleRegister)}>
        <TextField
        margin={'normal'}
        variant={"outlined"}
        label={'Email'}
        name={'email'}
        inputRef={register}
        size={'small'}
        error={!!errors.email}
        helperText={errors.email?.message}
        />
        <TextField
        margin={'normal'}
        variant={"outlined"}
        label={'Senha'}
        name={'password'}
        inputRef={register}
        size={'small'}
        error={!!errors.password}
        helperText={errors.password?.message}
        />
        <TextField
        margin={'normal'}
        variant={"outlined"}
        label={'Nome'}
        name={'name'}
        inputRef={register}
        size={'small'}
        error={!!errors.name}
        helperText={errors.name?.message}
        />
        <TextField
        margin={'normal'}
        variant={"outlined"}
        label={'Resumo'}
        name={'bio'}
        inputRef={register}
        size={'small'}
        error={!!errors.bio}
        helperText={errors.bio?.message}
        />
        <TextField
        margin={'normal'}
        variant={"outlined"}
        label={'Contato'}
        name={'contact'}
        inputRef={register}
        size={'small'}
        error={!!errors.contact}
        helperText={errors.contact?.message}
        />
        <TextField
        margin={'normal'}
        variant={"outlined"}
        label={'Módulo do curso'}
        name={'course_module'}
        inputRef={register}
        size={'small'}
        error={!!errors.course_module}
        helperText={errors.course_module?.message}
        />

        <FormButton type='submit'>Enviar</FormButton>
    </Form>
        </>
    )
}
export default RegisterForm