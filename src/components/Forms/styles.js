import styled from 'styled-components'

export const Form = styled.form`
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 200px;
  @media(min-width: 768px){
  width: 350px;
  }
`;

export const FormButton = styled.button`
    width: 150px;
    border-radius: 5px;
    background-color: #baff78;
    border: 1px solid lawngreen;
    font-size: 1rem;
    height: 40px;
  :hover{
    border: lawngreen 3px solid;
    //background-color: skyblue;
  }
`;