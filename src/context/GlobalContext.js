import {createContext, useEffect} from "react";
import {useLoading} from "./hooks/useLoading";
import {useAuth} from "./hooks/useAuth";
import baseURL from "../services";

const GlobalContext = createContext()

const GlobalProvider = ({children})=>{

    useEffect(()=>{
    const token = window.localStorage.getItem('token') || ''
    baseURL.defaults.headers.Authentication = `Bearer ${JSON.parse(token)}`
    },[])

    const {auth, isAuth} = useAuth()
    const {loading, setLoading} = useLoading()

    return(
        <GlobalContext.Provider value={{loading, auth}}>
            {children}
        </GlobalContext.Provider>
    )
}
export default GlobalProvider