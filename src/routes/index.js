import {Switch, Route} from 'react-router-dom'
import Register from "../pages/register";
import Login from "../pages/login";
import Home from "../pages/home";
import Root from "../pages/root";

const Routes = ()=>{

    return(
    <>
    <Switch>
        <Route exact path='/register'>
            <Register/>
        </Route>
        <Route exact path='/login'>
            <Login/>
        </Route>
        <Route exact path='/'>
            <Root/>
        </Route>
        {/*ROTA PRIVADA*/}
        <Route exact path='/home'>
            <Home/>
        </Route>
    </Switch>
    </>
    )
}

export default Routes