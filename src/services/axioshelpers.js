import baseURL from './index'

export const getUsers = async ()=>{
  const response = await baseURL.get('/users')
    console.log(response)
    return response.data
}

export const CreateUser = (data) =>{
    baseURL.post(`/users`, data).then((response) =>{
        console.log(response);
        return response;
    } ).catch(err => console.log(err))
}
